package azra.wildan.appx06

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragProdi : FragmentProdi
    lateinit var fragMhs : FragmentMahasiswa
    lateinit var fragMatkul : FragmentMatkul
    lateinit var fragNilai : FragmentNilai
    lateinit var ft : FragmentTransaction
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragProdi = FragmentProdi()
        fragMhs = FragmentMahasiswa()
        fragMatkul = FragmentMatkul()
        fragNilai = FragmentNilai()
        db = DBOpenHelper(this).writableDatabase
    }

    fun getDbObject() : SQLiteDatabase{
        return db
    }

        override fun onCreateOptionsMenu(menu: Menu?): Boolean {
            var mnuInflater = menuInflater
            mnuInflater.inflate(R.menu.menu_option,menu)
            return super.onCreateOptionsMenu(menu)
        }
        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            when(item?.itemId){
                R.id.itemMatkul ->{
                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.franeLayout,fragMatkul).commit()
                    franeLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                    franeLayout.visibility = View.VISIBLE
                }
                R.id.itemNilai ->{
                    ft = supportFragmentManager.beginTransaction()
                    ft.replace(R.id.franeLayout,fragNilai).commit()
                    franeLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                    franeLayout.visibility = View.VISIBLE
                }
            }
            return super.onOptionsItemSelected(item)
        }

        override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemProdi ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragProdi).commit()
                franeLayout.setBackgroundColor(Color.argb(245, 255, 255, 225))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemMhs ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.franeLayout,fragMhs).commit()
                franeLayout.setBackgroundColor(Color.argb(245, 225, 255, 255))
                franeLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout -> franeLayout.visibility = View.GONE
        }
        return true
    }
}
